function g = sigmoid(z)
%SIGMOID Compute sigmoid functoon
%   J = SIGMOID(z) computes the sigmoid of z.

% You need to return the following variables correctly 
g = zeros(size(z));

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the sigmoid of each value of z (z can be a matrix,
%               vector or scalar).

% ./ gives element-wise divide
% .+ gives element-wise addition
% this preserves the matrix/vector form if z happnes to be one

g = 1 ./ (1 .+ exp(-z));



% =============================================================

end
